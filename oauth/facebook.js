'use strict';
var config = require("../site-config.js").auth;
var passport = require('passport'),
    FacebookStrategy = require('passport-facebook').Strategy;

var FACEBOOK_APP_ID = config.facebook.FACEBOOK_APP_ID;
var FACEBOOK_APP_SECRET = config.facebook.FACEBOOK_APP_SECRET;

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(obj, done) {
    done(null, obj);
});


// Use the FacebookStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and Facebook
//   profile), and invoke a callback with a user object.
passport.use(new FacebookStrategy({
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET,
    //callbackURL: "http://drawlife.herokuapp.com:3000/auth/facebook/callback"
    callbackURL: "/auth/facebook/callback"
}, function(accessToken, refreshToken, profile, done) {
        // asynchronous verification, for effect...
    process.nextTick(function () {
        // To keep the example simple, the user's Facebook profile is returned to
        // represent the logged-in user.  In a typical application, you would want
        // to associate the Facebook account with a user record in your database,
        // and return that user instead.
        return done(null, profile);
    });
}
    ));
