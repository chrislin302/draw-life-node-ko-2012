/*
exports.mongo = {
    host: '127.0.0.1',
    port: 27017,
};
*/

exports.mongo = {
    'mongoDB': process.env.MONGOHQ_URL || 'mongodb://localhost/drawlife',
    'model': {
        'room': 'room',
        'account': 'account'
    }
} 

//exports.mongo = process.env.MONGOHQ_URL || 'mongodb://localhost/drawlife';

exports.site = {
    host: '', //    ex: http://127.0.0.1
    port: process.env.PORT || 3000,
    default_locales : 'zh-tw',
};

exports.auth = {
    facebook: {
        FACEBOOK_APP_ID: "551212248226405",
        FACEBOOK_APP_SECRET: "2c13dc304a4b613a53373fa2ced9fab2"
    }
};
