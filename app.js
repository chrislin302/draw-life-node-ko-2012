/**
 * Module dependencies.
 */
'use strict';

var express = require('express'),
    http = require('http'),
    parseCookie = require('connect').utils.parseCookie,
    MemoryStore = require('connect/lib/middleware/session/memory'),
    MongoStore = require('connect-mongo')(express),
    // MongoStore = require('connect-mongo')(express),
    setting = require('./site-config'),
    path = require('path'),
    locales = ['zh-cn', 'en-us', 'zh-tw'],
    // i18n = require("i18n"),
    utility = require('./utility'),
    passport = require('passport'),
    numCPUs = utility.getCpus() - 1,
    port = setting.site.port;

// Routes
var routes = require('./routes/index');

var app = express();
require('./oauth/facebook');

// var storeMemory = new MemoryStore({
//     reapInterval: 60000 * 10
// });

// Configuration
app.engine('.ejs', require('ejs').__express);

app.configure(function() {
    app.set('port', port);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');
    app.use(express.favicon());
    app.use(express.logger());
    app.use(express.cookieParser());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    // app.use(express.session({
    //     secret: 'co-drawlife',
    //     store: storeMemory
    // }));

    app.use(express.session({
        'secret': "@#$TYHBVGHJIY^TWEYKJHNBGFDWGHJKUYTWE#$%^&*&^%$#",
        // 'maxAge'  : new Date(Date.now() + 3600000), //1 Hour
        // 'expires' : new Date(Date.now() + 3600000), //1 Houri
        // 'cookie': {  path: '/', maxAge: 60000000 * 5 }
        'store': new MongoStore({
            'url': setting.mongo.mongoDB,
            'collection': 'sessions'
        })
    }));

    app.use(passport.initialize());
    app.use(passport.session());

    app.use(express.static(__dirname + '/public'));
    app.use(express.favicon("public/favicon.ico"));
    app.use(app.router);
});

app.configure('development', function() {
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
    app.locals.pretty = true;
});

app.configure('production', function() {
    app.use(express.errorHandler());
});


app.get('/', function (req, res) {
    res.sendfile(__dirname + '/public/index.html');
});


app.get('/room', routes.room);
app.post('/create', routes.create);
app.get('/logout', routes.logout);
app.post('/login', function(req, res){
    req.session.passport = {};
    req.session.passport.user = {};
    req.session.passport.user._json = {};
    req.session.passport.user._json.id = new Date().getTime() + Math.floor((Math.random()*(999-100+1)+100)).toString();
    req.session.passport.user._json.name = req.body.name;
    req.session.passport.user._json.link = 'http:/chris.oooo.tw/';
    req.session.passport.user._json.gender = 'male';
    res.redirect('/room.html');
});

app.post('/getuser', function(req, res) {
    res.send(req.session.passport.user._json)
    // console.log("---- ajax data ----");
    // console.log(req.body);
});

app.get('/auth/facebook', passport.authenticate('facebook'));

// GET /auth/facebook/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
app.get('/auth/facebook/callback', passport.authenticate('facebook', {
    failureRedirect: '/login'
}), function(req, res) {
    //console.log(req.session);
    req.session.passport.user._json.id = 'fb@' + req.session.passport.user._json.id;
    res.redirect('/room.html');
});

var server = http.createServer(app);

server.listen(port, function() {
    app.sockets = require('./socket.server')(server);
    console.log("Express server listening on port " + setting.site.port + " in " + process.env.NODE_ENV + " mode");
});
