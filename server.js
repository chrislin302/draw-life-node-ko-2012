var http = require('http');
var app = require('./app');
var server = http.createServer(app);
var port = app.get('port');


server.listen(port, function(){
  app.sockets = require('./socket.server')(server);
  console.log("Express server listening on port " + port);
});

